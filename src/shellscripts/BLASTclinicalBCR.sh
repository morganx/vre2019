#!/bin/sh
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR00128.fasta -out CLINICAL_BLASTOUTPUT/AR00128.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR0036.fasta -out CLINICAL_BLASTOUTPUT/AR0036.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR011155.fasta -out CLINICAL_BLASTOUTPUT/AR011155.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR01139.fasta -out CLINICAL_BLASTOUTPUT/AR01139.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR01215.fasta -out CLINICAL_BLASTOUTPUT/AR01215.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR01673.fasta -out CLINICAL_BLASTOUTPUT/AR01673.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR01802.fasta -out CLINICAL_BLASTOUTPUT/AR01802.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR01912.fasta -out CLINICAL_BLASTOUTPUT/AR01912.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR96360.fasta -out CLINICAL_BLASTOUTPUT/AR96360.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR98565.fasta -out CLINICAL_BLASTOUTPUT/AR98565.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR991024.fasta -out CLINICAL_BLASTOUTPUT/AR991024.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR991107.fasta -out CLINICAL_BLASTOUTPUT/AR991107.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR99190.fasta -out CLINICAL_BLASTOUTPUT/AR99190.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsAR9984.fasta -out CLINICAL_BLASTOUTPUT/AR9984.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL04446.fasta -out CLINICAL_BLASTOUTPUT/ARL04446.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL04506.fasta -out CLINICAL_BLASTOUTPUT/ARL04506.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL04840.fasta -out CLINICAL_BLASTOUTPUT/ARL04840.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL05425.fasta -out CLINICAL_BLASTOUTPUT/ARL05425.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL0567.fasta -out CLINICAL_BLASTOUTPUT/ARL0567.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071139.fasta -out CLINICAL_BLASTOUTPUT/ARL071139.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071163.fasta -out CLINICAL_BLASTOUTPUT/ARL071163.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071166.fasta -out CLINICAL_BLASTOUTPUT/ARL071166.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071169.fasta -out CLINICAL_BLASTOUTPUT/ARL071169.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071195.fasta -out CLINICAL_BLASTOUTPUT/ARL071195.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071198.fasta -out CLINICAL_BLASTOUTPUT/ARL071198.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071205.fasta -out CLINICAL_BLASTOUTPUT/ARL071205.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071286.fasta -out CLINICAL_BLASTOUTPUT/ARL071286.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071300.fasta -out CLINICAL_BLASTOUTPUT/ARL071300.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071328.fasta -out CLINICAL_BLASTOUTPUT/ARL071328.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071345.fasta -out CLINICAL_BLASTOUTPUT/ARL071345.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071348.fasta -out CLINICAL_BLASTOUTPUT/ARL071348.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071351.fasta -out CLINICAL_BLASTOUTPUT/ARL071351.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL071384.fasta -out CLINICAL_BLASTOUTPUT/ARL071384.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL07973.fasta -out CLINICAL_BLASTOUTPUT/ARL07973.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08105.fasta -out CLINICAL_BLASTOUTPUT/ARL08105.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL081074.fasta -out CLINICAL_BLASTOUTPUT/ARL081074.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL081077.fasta -out CLINICAL_BLASTOUTPUT/ARL081077.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL081189.fasta -out CLINICAL_BLASTOUTPUT/ARL081189.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL081215.fasta -out CLINICAL_BLASTOUTPUT/ARL081215.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL081218.fasta -out CLINICAL_BLASTOUTPUT/ARL081218.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL081249.fasta -out CLINICAL_BLASTOUTPUT/ARL081249.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL081256.fasta -out CLINICAL_BLASTOUTPUT/ARL081256.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL081292.fasta -out CLINICAL_BLASTOUTPUT/ARL081292.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL081309.fasta -out CLINICAL_BLASTOUTPUT/ARL081309.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL081412.fasta -out CLINICAL_BLASTOUTPUT/ARL081412.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL081437.fasta -out CLINICAL_BLASTOUTPUT/ARL081437.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08192.fasta -out CLINICAL_BLASTOUTPUT/ARL08192.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08288.fasta -out CLINICAL_BLASTOUTPUT/ARL08288.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL0832.fasta -out CLINICAL_BLASTOUTPUT/ARL0832.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08339.fasta -out CLINICAL_BLASTOUTPUT/ARL08339.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL0835.fasta -out CLINICAL_BLASTOUTPUT/ARL0835.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08391.fasta -out CLINICAL_BLASTOUTPUT/ARL08391.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08394.fasta -out CLINICAL_BLASTOUTPUT/ARL08394.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL0839.fasta -out CLINICAL_BLASTOUTPUT/ARL0839.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08428.fasta -out CLINICAL_BLASTOUTPUT/ARL08428.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL0843.fasta -out CLINICAL_BLASTOUTPUT/ARL0843.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL0846.fasta -out CLINICAL_BLASTOUTPUT/ARL0846.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08477.fasta -out CLINICAL_BLASTOUTPUT/ARL08477.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08484.fasta -out CLINICAL_BLASTOUTPUT/ARL08484.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08552.fasta -out CLINICAL_BLASTOUTPUT/ARL08552.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL0867.fasta -out CLINICAL_BLASTOUTPUT/ARL0867.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08712.fasta -out CLINICAL_BLASTOUTPUT/ARL08712.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08781.fasta -out CLINICAL_BLASTOUTPUT/ARL08781.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08804.fasta -out CLINICAL_BLASTOUTPUT/ARL08804.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08902.fasta -out CLINICAL_BLASTOUTPUT/ARL08902.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL08983.fasta -out CLINICAL_BLASTOUTPUT/ARL08983.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL09100.fasta -out CLINICAL_BLASTOUTPUT/ARL09100.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL091074a.fasta -out CLINICAL_BLASTOUTPUT/ARL091074a.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL091209.fasta -out CLINICAL_BLASTOUTPUT/ARL091209.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL091265.fasta -out CLINICAL_BLASTOUTPUT/ARL091265.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL091374.fasta -out CLINICAL_BLASTOUTPUT/ARL091374.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL09146.fasta -out CLINICAL_BLASTOUTPUT/ARL09146.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL09193.fasta -out CLINICAL_BLASTOUTPUT/ARL09193.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL09409.fasta -out CLINICAL_BLASTOUTPUT/ARL09409.txt -outfmt 6
blastn -db nooperon_BcrRABD_fasta_format.fasta -query scaffoldsARL09542.fasta -out CLINICAL_BLASTOUTPUT/ARL09542.txt -outfmt 6

























































