#!/bin/sh
blastn -db qacC.fa -query scaffoldsAR00128.fasta -out CLINICAL_BLAST_qacC/AR00128.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR0036.fasta -out CLINICAL_BLAST_qacC/AR0036.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR011155.fasta -out CLINICAL_BLAST_qacC/AR011155.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR01139.fasta -out CLINICAL_BLAST_qacC/AR01139.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR01215.fasta -out CLINICAL_BLAST_qacC/AR01215.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR01673.fasta -out CLINICAL_BLAST_qacC/AR01673.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR01802.fasta -out CLINICAL_BLAST_qacC/AR01802.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR01912.fasta -out CLINICAL_BLAST_qacC/AR01912.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR96360.fasta -out CLINICAL_BLAST_qacC/AR96360.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR98565.fasta -out CLINICAL_BLAST_qacC/AR98565.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR991024.fasta -out CLINICAL_BLAST_qacC/AR991024.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR991107.fasta -out CLINICAL_BLAST_qacC/AR991107.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR99190.fasta -out CLINICAL_BLAST_qacC/AR99190.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsAR9984.fasta -out CLINICAL_BLAST_qacC/AR9984.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL04446.fasta -out CLINICAL_BLAST_qacC/ARL04446.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL04506.fasta -out CLINICAL_BLAST_qacC/ARL04506.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL04840.fasta -out CLINICAL_BLAST_qacC/ARL04840.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL05425.fasta -out CLINICAL_BLAST_qacC/ARL05425.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL0567.fasta -out CLINICAL_BLAST_qacC/ARL0567.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071139.fasta -out CLINICAL_BLAST_qacC/ARL071139.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071163.fasta -out CLINICAL_BLAST_qacC/ARL071163.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071166.fasta -out CLINICAL_BLAST_qacC/ARL071166.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071169.fasta -out CLINICAL_BLAST_qacC/ARL071169.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071195.fasta -out CLINICAL_BLAST_qacC/ARL071195.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071198.fasta -out CLINICAL_BLAST_qacC/ARL071198.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071205.fasta -out CLINICAL_BLAST_qacC/ARL071205.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071286.fasta -out CLINICAL_BLAST_qacC/ARL071286.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071300.fasta -out CLINICAL_BLAST_qacC/ARL071300.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071328.fasta -out CLINICAL_BLAST_qacC/ARL071328.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071345.fasta -out CLINICAL_BLAST_qacC/ARL071345.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071348.fasta -out CLINICAL_BLAST_qacC/ARL071348.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071351.fasta -out CLINICAL_BLAST_qacC/ARL071351.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL071384.fasta -out CLINICAL_BLAST_qacC/ARL071384.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL07973.fasta -out CLINICAL_BLAST_qacC/ARL07973.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08105.fasta -out CLINICAL_BLAST_qacC/ARL08105.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL081074.fasta -out CLINICAL_BLAST_qacC/ARL081074.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL081077.fasta -out CLINICAL_BLAST_qacC/ARL081077.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL081189.fasta -out CLINICAL_BLAST_qacC/ARL081189.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL081215.fasta -out CLINICAL_BLAST_qacC/ARL081215.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL081218.fasta -out CLINICAL_BLAST_qacC/ARL081218.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL081249.fasta -out CLINICAL_BLAST_qacC/ARL081249.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL081256.fasta -out CLINICAL_BLAST_qacC/ARL081256.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL081292.fasta -out CLINICAL_BLAST_qacC/ARL081292.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL081309.fasta -out CLINICAL_BLAST_qacC/ARL081309.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL081412.fasta -out CLINICAL_BLAST_qacC/ARL081412.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL081437.fasta -out CLINICAL_BLAST_qacC/ARL081437.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08192.fasta -out CLINICAL_BLAST_qacC/ARL08192.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08288.fasta -out CLINICAL_BLAST_qacC/ARL08288.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL0832.fasta -out CLINICAL_BLAST_qacC/ARL0832.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08339.fasta -out CLINICAL_BLAST_qacC/ARL08339.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL0835.fasta -out CLINICAL_BLAST_qacC/ARL0835.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08391.fasta -out CLINICAL_BLAST_qacC/ARL08391.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08394.fasta -out CLINICAL_BLAST_qacC/ARL08394.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL0839.fasta -out CLINICAL_BLAST_qacC/ARL0839.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08428.fasta -out CLINICAL_BLAST_qacC/ARL08428.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL0843.fasta -out CLINICAL_BLAST_qacC/ARL0843.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL0846.fasta -out CLINICAL_BLAST_qacC/ARL0846.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08477.fasta -out CLINICAL_BLAST_qacC/ARL08477.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08484.fasta -out CLINICAL_BLAST_qacC/ARL08484.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08552.fasta -out CLINICAL_BLAST_qacC/ARL08552.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL0867.fasta -out CLINICAL_BLAST_qacC/ARL0867.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08712.fasta -out CLINICAL_BLAST_qacC/ARL08712.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08781.fasta -out CLINICAL_BLAST_qacC/ARL08781.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08804.fasta -out CLINICAL_BLAST_qacC/ARL08804.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08902.fasta -out CLINICAL_BLAST_qacC/ARL08902.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL08983.fasta -out CLINICAL_BLAST_qacC/ARL08983.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL09100.fasta -out CLINICAL_BLAST_qacC/ARL09100.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL091074a.fasta -out CLINICAL_BLAST_qacC/ARL091074a.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL091209.fasta -out CLINICAL_BLAST_qacC/ARL091209.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL091265.fasta -out CLINICAL_BLAST_qacC/ARL091265.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL091374.fasta -out CLINICAL_BLAST_qacC/ARL091374.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL09146.fasta -out CLINICAL_BLAST_qacC/ARL09146.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL09193.fasta -out CLINICAL_BLAST_qacC/ARL09193.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL09409.fasta -out CLINICAL_BLAST_qacC/ARL09409.txt -outfmt 6
blastn -db qacC.fa -query scaffoldsARL09542.fasta -out CLINICAL_BLAST_qacC/ARL09542.txt -outfmt 6
